<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\DealersBundle\Provider;

use Xngage\Bundle\DealersBundle\Exception\GeoCodingException;
use GuzzleHttp\Client;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Oro\Component\MessageQueue\Util\JSON;
use Psr\Log\LoggerInterface;

class GeoCodingProvider
{
    const BASE_URL = 'https://maps.googleapis.com/maps/api/geocode/';
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var ConfigManager
     */
    protected $configManager;

    /**
     * @var ConfigManager
     */
    protected $logger;

    /**
     * Google Geocoding Service API Key
     * @var string
     */
    protected $apiKey;

    /**
     * GeoCodingProvider constructor.
     * @param ConfigManager $manager
     * @param LoggerInterface $logger
     */
    public function __construct(ConfigManager $manager, LoggerInterface $logger)
    {
        $this->configManager = $manager;
        $this->logger = $logger;
    }

    /**
     * Init the HTTP client and fetch the API key from config
     */
    public function init()
    {
        $this->client = new Client([
            'base_uri' => self::BASE_URL,
            'timeout' => 0,
            'allow_redirects' => false
        ]);

        $this->apiKey = $this->configManager->get('oro_google_integration.google_api_key');

        if (!$this->apiKey) {
            throw new \RuntimeException('Stockists GeoCode API Key has not been set in configuration.');
        }
    }

    /**
     * Query the Google GeoCoding API to convert an address string
     * @param $addressString
     * @return array
     */
    public function geoCodeAddress($addressString)
    {
        if (!$this->client) {
            $this->init();
        }

        $request = $this->client->get(
            'json',
            [
                'query' => [
                    'address' => $addressString
                ]
            ]
        );

        if ($request->getStatusCode() == 200) {
            return JSON::decode($request->getBody()->getContents());
        } else {
            throw new \RuntimeException(
                "An error occurred while contacting the geocoding API.
                {{ $response->getStatusCode() }} - {{ $response->getMessage() }}"
            );
        }
    }

    /**
     * Example request url:
     * https://maps.googleapis.com/maps/api/geocode/json?address=5000%20adelaide&components=postal_code:5000|country
     * :AU&key=<googleApiKey>
     * API Docs: https://developers.google.com/maps/documentation/geocoding/intro#ComponentFiltering
     * @param $postCode
     * @param $address
     * @param string $country
     * @return array
     */
    public function geoCodePostCodeOrFullAddress($address, $postCode, $country = 'US')
    {
        if (!$this->client) {
            $this->init();
        }

        $requestOptions = $this->getRequestOptions($address, $postCode, $country);

        $request = $this->client->get(
            'json',
            $requestOptions
        );

        if ($request->getStatusCode() == 200) {
            return JSON::decode($request->getBody()->getContents());
        } else {

            throw new \RuntimeException(
                "An error occurred while contacting the geocoding API.
                {{ $response->getStatusCode() }} - {{ $response->getMessage() }}"
            );
        }
    }

    /**
     * Geocodes the Passed address string and returns the lat and long
     * @param $address
     * @return array
     */
    public function getLatLongFromAddress($address)
    {
        $data = $this->geoCodeAddress($address);

        if (isset($data['status']) && $data['status'] === 'OK') {
            $addressData = reset($data['results']);
            if ($addressData) {
                if (isset($addressData['geometry']['location'])) {
                    $output['lat'] = $addressData['geometry']['location']['lat'];
                    $output['long'] = $addressData['geometry']['location']['lng'];
                    return $output;
                }
            }
        }

        throw new \InvalidArgumentException("Address [$address] failed to decode");
    }

    /**
     * @param $address
     * @param $postCode
     * @param $country
     * @return array
     */
    public function getLatLngFromAdressOrPostCode($address, $postCode, $country)
    {
        $data = $this->geoCodePostCodeOrFullAddress($address, $postCode, $country);

        if (isset($data['status']) && $data['status'] === 'OK') {
            $addressData = reset($data['results']);
            if ($addressData) {
                if (isset($addressData['geometry']['location'])) {
                    $output['lat'] = $addressData['geometry']['location']['lat'];
                    $output['long'] = $addressData['geometry']['location']['lng'];
                    return $output;
                }
            }
        }

        $this->logger->warning("Postcode [$postCode] and address [$address] failed to decode");
        throw new GeoCodingException("Postcode [$postCode] and address [$address] failed to decode");
    }

    /**
     * Fixes: https://charlesparson.atlassian.net/browse/ACPO-675
     * Creates request options, depending on if just the postcode or address is set
     * only the address or the postcode option is allowed to be set to get good results
     * @param $address
     * @param $postCode
     * @param string $country
     * @return array
     */
    protected function getRequestOptions($address, $postCode, string $country)
    {
        if ($postCode) {
            //Request options with postcode
            return [
                'query' => [
                    'key' => $this->apiKey,
                    'components' => implode(
                        '|', //components separated by pipe operator as specified per google api
                        [
                            "postal_code:$postCode",
                            "country:$country"
                        ]
                    )
                ]
            ];
        } elseif ($address && !$postCode) {
            //Request options with address
            return [
                'query' => [
                    'address' => $address,
                    'key' => $this->apiKey,
                    'components' => implode(
                        '|', //components separated by pipe operator as specified per google api
                        [
                            "country:$country"
                        ]
                    )
                ]
            ];
        } else {
            throw new \InvalidArgumentException('postcode or address and not postcode is allowed to be set');
        }
    }
}
