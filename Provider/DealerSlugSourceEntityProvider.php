<?php

namespace Xngage\Bundle\DealersBundle\Provider;

use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Oro\Bundle\RedirectBundle\DependencyInjection\Configuration;
use Oro\Bundle\RedirectBundle\Entity\Slug;
use Oro\Bundle\RedirectBundle\Entity\SlugAwareInterface;
use Oro\Bundle\RedirectBundle\Provider\SluggableEntityFinder;
use Oro\Bundle\RedirectBundle\Provider\SlugSourceEntityProviderInterface;
use Xngage\Bundle\DealersBundle\Entity\Dealer;

/**
 * Provides Dealer source entity for the slug.
 */
class DealerSlugSourceEntityProvider implements SlugSourceEntityProviderInterface
{
    private SluggableEntityFinder $sluggableEntityFinder;
    private ConfigManager $configManager;

    public function __construct(SluggableEntityFinder $sluggableEntityFinder, ConfigManager $configManager)
    {
        $this->sluggableEntityFinder = $sluggableEntityFinder;
        $this->configManager = $configManager;
    }

    /**
     * {@inheritDoc}
     */
    public function getSourceEntityBySlug(Slug $slug): ?SlugAwareInterface
    {
        if (!$this->configManager->get(Configuration::getConfigKey(Configuration::ENABLE_DIRECT_URL))) {
            return null;
        }

        return $this->sluggableEntityFinder->findEntityBySlug(Dealer::class, $slug);
    }
}
