<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\DealersBundle\Provider;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;

class DealersProvider
{
    const SESSION_KEY = 'xngage-frontend-dealers-grid-km-or-mi';
 
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * @var ConfigManager
     */
    protected $configManager;

    /**
     * DealersProvider constructor.
     * @param ConfigManager $configManager
     * @param RequestStack $requestStack
     * @param SessionInterface $session
     */
    public function __construct(
        ConfigManager $configManager,
        RequestStack $requestStack,
        SessionInterface $session
    ) {
        $this->requestStack = $requestStack;
        $this->session = $session;
        $this->configManager = $configManager;
    }

    public function getCurrentDistanceKmOrMi()
    {
        $request = $this->requestStack->getCurrentRequest();
        $mi_or_km = $this->configManager->get('xngage_dealers.miles_or_kilometers', 'miles');

        if ($request) {
            $gridParams = $request->query->get('xngage-frontend-dealers-grid');
            if (is_array($gridParams) && array_key_exists('mi_or_km', $gridParams)) {
                $mi_or_km = $gridParams['mi_or_km'];
                if (!in_array($mi_or_km, ['kilometers', 'miles'])) {
                    $mi_or_km = $this->configManager->get('xngage_dealers.miles_or_kilometers', 'miles');
                }
                $this->session->set(self::SESSION_KEY, $mi_or_km);
            } elseif ($this->session->has(self::SESSION_KEY)) {
                $mi_or_km = $this->session->get(self::SESSION_KEY);
            }
        }

        return $mi_or_km;
    }
}
