<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\DealersBundle\Provider;

use Oro\Bundle\AddressBundle\Entity\Address;
use Oro\Bundle\SecurityBundle\Authentication\TokenAccessorInterface;
use Oro\Bundle\ShippingBundle\Provider\ShippingOriginProvider;

class LatLongProvider implements LatLongProviderInterface
{
    /**
     * @var GeoCodingProvider
     */
    protected $geoCodingProvider;

    /**
     * @var TokenAccessorInterface
     */
    protected TokenAccessorInterface $tokenAccessor;

    /**
     * @var ShippingOriginProvider
     */
    protected $shippingOriginProvider;

    /**
     * LatLongProvider constructor.
     * @param GeoCodingProvider $geoCodingProvider
     * @param TokenAccessorInterface $tokenAccessor
     */
    public function __construct(
        GeoCodingProvider $geoCodingProvider,
        TokenAccessorInterface $tokenAccessor,
        ShippingOriginProvider $shippingOriginProvider
    ) {
        $this->geoCodingProvider = $geoCodingProvider;
        $this->tokenAccessor = $tokenAccessor;
        $this->shippingOriginProvider = $shippingOriginProvider;
    }

    /**
     * @param string $postcode
     * @param string $address
     * @param string $country
     * @return array
     */
    public function findLatLong(string $address, ?string $postcode, ?string $country)
    {
        /** @var array $formattedAddress */
        $formattedAddress = $this->geoCodingProvider->getLatLngFromAdressOrPostCode($address, $postcode, $country);
        $latLong = [];

        if (isset($formattedAddress['lat']) && isset($formattedAddress['long'])) {
            $latLong['lat'] = $formattedAddress['lat'];
            $latLong['long'] = $formattedAddress['long'];
        }

        return $latLong;
    }

    public function findLatLongFromCustomerAddress()
    {
        $currentCustomerUser = $this->tokenAccessor->getUser();
        if (!$currentCustomerUser) {
            return $this->getAddressFromShippingOrigin();
        }

        /** @var Address $address */
        $address = $currentCustomerUser->getAddressByTypeName('shipping');

        if (!$address) {
            $address = $currentCustomerUser->getAddressByTypeName('billing');
        }

        if (!$address) {
            // Get address from customer
            $customer = $currentCustomerUser->getCustomer();
            $address = $customer->getAddressByTypeName('shipping');

            if (!$address) {
                $address = $customer->getAddressByTypeName('billing');
            }
        }

        if (!$address) {
            return $this->getAddressFromShippingOrigin();
        }

        return $this->findLatLong($address->getStreet(), $address->getPostalCode(), $address->getCountryIso2());
    }

    private function getAddressFromShippingOrigin()
    {
        $shippingOrigin = $this->shippingOriginProvider->getSystemShippingOrigin();
        if (null !== $shippingOrigin && $shippingOrigin->getStreet() && $shippingOrigin->getPostalCode()) {
            return $this->findLatLong($shippingOrigin->getStreet(), $shippingOrigin->getPostalCode(), $shippingOrigin->getCountry() ? $shippingOrigin->getCountry()->getIso2Code() : "us");
        }

        return [];
    }
}
