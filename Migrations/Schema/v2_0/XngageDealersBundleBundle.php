<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\DealersBundle\Migrations\Schema\v2_0;

use Doctrine\DBAL\Schema\Schema;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

class XngageDealersBundleBundle implements 
    Migration
{
    const TABLE_NAME = 'xngage_dealer';

    /**
     * {@inheritdoc}
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        /** Tables generation **/
        $this->createXngageDealerWebsiteTable($schema);
        $this->addXngageDealerWebsiteForeignKeys($schema);
    }

    /**
     * Create xngage_dealer_website table
     */
    protected function createXngageDealerWebsiteTable(Schema $schema)
    {
        $table = $schema->createTable('xngage_dealer_website');
        $table->addColumn('dealer_id', 'integer', []);
        $table->addColumn('website_id', 'integer', []);
        $table->setPrimaryKey(['dealer_id', 'website_id']);
        $table->addIndex(['dealer_id'], 'IDX_7EE0544ESDVBB5ED3', []);
    }

    /**
     * Add xngage_dealer_website foreign keys.
     */
    protected function addXngageDealerWebsiteForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('xngage_dealer_website');
        $table->addForeignKeyConstraint(
            $schema->getTable(self::TABLE_NAME),
            ['dealer_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_website'),
            ['website_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
    }
}