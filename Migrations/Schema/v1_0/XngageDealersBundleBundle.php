<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\DealersBundle\Migrations\Schema\v1_0;

use Oro\Bundle\AttachmentBundle\Migration\Extension\AttachmentExtensionAwareInterface;
use Oro\Bundle\AttachmentBundle\Migration\Extension\AttachmentExtensionAwareTrait;
use Doctrine\DBAL\Schema\Schema;
use Oro\Bundle\ActivityBundle\Migration\Extension\ActivityExtension;
use Oro\Bundle\ActivityBundle\Migration\Extension\ActivityExtensionAwareInterface;
use Oro\Bundle\EntityExtendBundle\Migration\Extension\ExtendExtensionAwareInterface;
use Oro\Bundle\EntityExtendBundle\Migration\Extension\ExtendExtension;
use Oro\Bundle\EntityExtendBundle\EntityConfig\ExtendScope;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

class XngageDealersBundleBundle implements 
    Migration, 
    ActivityExtensionAwareInterface,
    ExtendExtensionAwareInterface,
    AttachmentExtensionAwareInterface
{
    use AttachmentExtensionAwareTrait;

    const TABLE_NAME = 'xngage_dealer';
    const MAX_IMAGE_SIZE_IN_MB = 10;
    const THUMBNAIL_WIDTH_SIZE_IN_PX = 200;
    const THUMBNAIL_HEIGHT_SIZE_IN_PX = 200;
    const MIME_TYPES = [
        'image/gif',
        'image/jpeg',
        'image/png',
        'image/svg+xml'
    ];

    /** @var ActivityExtension */
    protected $activityExtension;

    /**
     * {@inheritdoc}
     */
    public function setActivityExtension(ActivityExtension $activityExtension)
    {
        $this->activityExtension = $activityExtension;
    }

    /** @var ExtendExtension */
    protected $extendExtension;

    /**
     * @inheritdoc
     */
    public function setExtendExtension(ExtendExtension $extendExtension)
    {
        $this->extendExtension = $extendExtension;
    }

    /**
     * {@inheritdoc}
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        /** Tables generation **/
        $this->createDealerTable($schema);
        $this->createDealerNameTable($schema);
        $this->createDealerDescriptionTable($schema);
        $this->addDealerNameForeignKeys($schema);
        $this->addDealerDescriptionForeignKeys($schema);

        $this->addImageAssociation($schema, 'image1', self::MIME_TYPES);
        $this->addImageAssociation($schema, 'image2', self::MIME_TYPES);
        $this->addImageAssociation($schema, 'image3', self::MIME_TYPES);
        $this->addImageAssociation($schema, 'managerImage', self::MIME_TYPES);
    }

    /**
     * Create dealer table
     *
     * @param Schema $schema
     */
    protected function createDealerTable(Schema $schema)
    {
        $table = $schema->createTable(self::TABLE_NAME);
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('erb_id', 'integer', ['notnull' => false]);
        $table->addColumn('active', 'boolean', ['default' => true]);
        $table->addColumn('phone', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('address_1', 'string', ['length' => 255]);
        $table->addColumn('address_2', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('country_code', 'string', ['notnull' => false, 'length' => 2]);
        $table->addColumn('city', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('postal_code', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('region_code', 'string', ['notnull' => false, 'length' => 16]); // label state
        $table->addColumn('website_url', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('hours', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('manager_name', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('manager_cell_phone', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('manager_email', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('manager_office_phoe', 'string', ['notnull' => false, 'length' => 255]);
        $table->addColumn('lat', 'decimal', ['notnull' => false, 'precision' => 18, 'scale' => 12]);
        $table->addColumn('lng', 'decimal', ['notnull' => false, 'precision' => 18, 'scale' => 12]);
        $table->addColumn('created_at', 'datetime', []);
        $table->addColumn('updated_at', 'datetime', []);
        $table->addIndex(['erb_id'], 'idx_1b9c3434fdcsc3f3', []);
        $table->setPrimaryKey(['id']);
        $table->addIndex(['updated_at'], 'idx_xngage_dealer_updated_at', []);
        $table->addIndex(['created_at'], 'idx_xngage_dealer_created_at', []);
    }

    /**
     * @param Schema $schema
     * @param string $fieldName
     * @param array  $mimeTypes
     */
    protected function addImageAssociation(Schema $schema, $fieldName, array $mimeTypes = [])
    {
        $this->attachmentExtension->addImageRelation(
            $schema,
            self::TABLE_NAME,
            $fieldName,
            [
                'attachment' => [
                    'acl_protected' => false,
                ],
                'importexport' => ['excluded' => true],
            ],
            self::MAX_IMAGE_SIZE_IN_MB,
            self::THUMBNAIL_WIDTH_SIZE_IN_PX,
            self::THUMBNAIL_HEIGHT_SIZE_IN_PX,
            $mimeTypes
        );
    }

    protected function createDealerNameTable(Schema $schema)
    {
        $table = $schema->createTable('xngage_dealer_name');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('dealer_id', 'integer', ['notnull' => false]);
        $table->addColumn('localization_id', 'integer', ['notnull' => false]);
        $table->addColumn('fallback', 'string', ['notnull' => false, 'length' => 64]);
        $table->addColumn('string', 'string', ['notnull' => false, 'length' => 255]);
        $table->setPrimaryKey(['id']);
        $table->addIndex(['fallback'], 'idx_xngage_dealer_fallback', []);
        $table->addIndex(['string'], 'idx_xngage_dealer_string', []);
    }

    protected function createDealerDescriptionTable(Schema $schema)
    {
        $table = $schema->createTable('xngage_dealer_description');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('dealer_id', 'integer', ['notnull' => false]);
        $table->addColumn('localization_id', 'integer', ['notnull' => false]);
        $table->addColumn('fallback', 'string', ['notnull' => false, 'length' => 64]);
        $table->addColumn('text', 'text', ['notnull' => false]);
        $table->setPrimaryKey(['id']);
        $table->addIndex(['fallback'], 'idx_xngage_dealer_s_descr_fallback', []);
    }

    protected function addDealerNameForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('xngage_dealer_name');
        $table->addForeignKeyConstraint(
            $schema->getTable(self::TABLE_NAME),
            ['dealer_id'],
            ['id'],
            ['onUpdate' => null, 'onDelete' => 'CASCADE']
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_localization'),
            ['localization_id'],
            ['id'],
            ['onUpdate' => null, 'onDelete' => 'CASCADE']
        );
    }

    protected function addDealerDescriptionForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('xngage_dealer_description');
        $table->addForeignKeyConstraint(
            $schema->getTable(self::TABLE_NAME),
            ['dealer_id'],
            ['id'],
            ['onUpdate' => null, 'onDelete' => 'CASCADE']
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_localization'),
            ['localization_id'],
            ['id'],
            ['onUpdate' => null, 'onDelete' => 'CASCADE']
        );
    }
}