<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\DealersBundle\Datagrid\Filters;

use Oro\Bundle\FilterBundle\Filter\StringFilter;
use Oro\Bundle\FilterBundle\Datasource\FilterDatasourceAdapterInterface;
use Oro\Bundle\FilterBundle\Datasource\Orm\OrmFilterDatasourceAdapter;

class FullSearchFilter extends StringFilter
{
    /**
     * @inheritdoc
     */
    public function apply(FilterDatasourceAdapterInterface $ds, $data)
    {
        /** @var array $data */
        $data = $this->parseData($data);
       
        if (!$data) {
            return false;
        }

        if (strlen($data['value']) < 2) return false;

        $this->restrict($ds, $data['value']);

        return true;
    }

    /**
     * @param OrmFilterDatasourceAdapter|FilterDatasourceAdapterInterface $ds
     * @param string $value
     */
    public function restrict($ds, $value)
    {
        $valueLike = '%' . strtolower($value) . '%';
        $queryBuilder = $ds->getQueryBuilder();
        $queryBuilder->where(
            $queryBuilder->expr()->orX(
                $queryBuilder->expr()->like('LOWER(dealer.title)', ':title'),
                $queryBuilder->expr()->like('LOWER(dealer.city)', ':city'),
                $queryBuilder->expr()->eq('LOWER(dealer.postalCode)', ":postalCode"),
                $queryBuilder->expr()->like('LOWER(re.name)', ":regionName")
            )
        )->leftJoin('dealer.region', 're')
        ->setParameters([
            "title" => $valueLike,
            "city" => $valueLike,
            "postalCode" => $value,
            "regionName" => $valueLike,
        ]);
    }
}