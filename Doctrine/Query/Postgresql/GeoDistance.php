<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\DealersBundle\Doctrine\Query\Postgresql;

use Xngage\Bundle\DealersBundle\Doctrine\Query\Mysql\GeoDistance as BaseGeoDistance;

/**
 * {@inheritDoc}
 */
class GeoDistance extends BaseGeoDistance {

    protected function getSqlWithPlaceholders() {
        return '%s * ASIN(SQRT(POWER(SIN((CAST(%s AS numeric) - CAST(%s AS numeric)) * PI()/360), 2) + COS(%s * PI()/180) * COS(%s * PI()/180) * POWER(SIN((CAST(%s AS numeric) - CAST(%s AS numeric)) * PI()/360), 2)))';
    }

}