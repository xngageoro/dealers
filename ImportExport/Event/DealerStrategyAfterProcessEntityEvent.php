<?php

namespace Xngage\Bundle\DealersBundle\ImportExport\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Xngage\Bundle\DealersBundle\Entity\Dealer;

/**
 * Holds the dealer object and import data. Fires on "after process entity" step in the import/export strategy.
 */
class DealerStrategyAfterProcessEntityEvent extends Event
{
    /** @var Dealer */
    protected $dealer;

    /** @var array */
    protected $rawData;

    public function __construct(Dealer $dealer, array $rawData)
    {
        $this->dealer = $dealer;
        $this->rawData = $rawData;
    }

    public function getDealer(): Dealer
    {
        return $this->dealer;
    }

    public function getRawData(): array
    {
        return $this->rawData;
    }
}
