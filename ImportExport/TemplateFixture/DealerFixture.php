<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\DealersBundle\ImportExport\TemplateFixture;

use Oro\Bundle\ImportExportBundle\TemplateFixture\AbstractTemplateRepository;
use Oro\Bundle\ImportExportBundle\TemplateFixture\TemplateFixtureInterface;
use Xngage\Bundle\DealersBundle\Entity\Dealer;
use Xngage\Bundle\DealersBundle\Entity\DealerTitle;

class DealerFixture extends AbstractTemplateRepository implements TemplateFixtureInterface
{
    public function getEntityClass()
    {
        return 'Xngage\Bundle\DealersBundle\Entity\Dealer';
    }

    public function getData()
    {
        return $this->getEntityData('example-xngage-dealer-data');
    }

    public function fillEntityData($key,$entity)
    {
        $title = (new DealerTitle())->setDealer($entity);
        $title->setString("Location Name");
        $entity->addTitle($title);
        
    }

    protected function createEntity($key)
    {
        return new Dealer();
    }
}