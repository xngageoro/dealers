<?php

namespace Xngage\Bundle\DealersBundle\ImportExport\Strategy;

use Xngage\Bundle\DealersBundle\Entity\Dealer;
use Xngage\Bundle\DealersBundle\ImportExport\Event\DealerStrategyAfterProcessEntityEvent;
use Oro\Bundle\LocaleBundle\ImportExport\Strategy\LocalizedFallbackValueAwareStrategy;

/**
 * Import strategy for Dealer.
 *
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class DealerAddOrReplaceStrategy extends LocalizedFallbackValueAwareStrategy
{
    /**
     * {@inheritdoc}
     *
     * @param Dealer $dealer
     */
    protected function afterProcessEntity($dealer)
    {
        $this->eventDispatcher->dispatch(
            new DealerStrategyAfterProcessEntityEvent($dealer, $this->context->getValue('itemData'))
        );

        return $dealer;
    }
}
