<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\DealersBundle\ImportExport\Configuration;

use Oro\Bundle\ImportExportBundle\Configuration\ImportExportConfiguration;
use Oro\Bundle\ImportExportBundle\Configuration\ImportExportConfigurationInterface;
use Oro\Bundle\ImportExportBundle\Configuration\ImportExportConfigurationProviderInterface;
use Xngage\Bundle\DealersBundle\Entity\Dealer;

/**
 * Import/export configuration provider for dealers.
 */
class DealerImportExportConfigurationProvider implements ImportExportConfigurationProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function get(): ImportExportConfigurationInterface
    {
        return new ImportExportConfiguration(
            [
                ImportExportConfiguration::FIELD_ENTITY_CLASS => Dealer::class,
                ImportExportConfiguration::FIELD_EXPORT_PROCESSOR_ALIAS => 'xngage_dealer_import_export',
                ImportExportConfiguration::FIELD_EXPORT_TEMPLATE_PROCESSOR_ALIAS => 'xngage_dealer_import_export',
                ImportExportConfiguration::FIELD_IMPORT_PROCESSOR_ALIAS => 'xngage_dealer_import_export',
                ImportExportConfiguration::FIELD_IMPORT_JOB_NAME => 'xngage_import_xngage_dealer_import_export_from_csv'
            ]
        );
    }
}
