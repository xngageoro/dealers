<?php

namespace Xngage\Bundle\DealersBundle\ImportExport\DataConverter;

use Oro\Bundle\LocaleBundle\ImportExport\DataConverter\LocalizedFallbackValueAwareDataConverter;

/**
 * Data converter for dealer.
 */
class DealerDataConverter extends LocalizedFallbackValueAwareDataConverter
{
}
