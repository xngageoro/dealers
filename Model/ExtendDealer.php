<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\DealersBundle\Model;

use Oro\Bundle\AttachmentBundle\Entity\File;
use Oro\Bundle\LocaleBundle\Entity\Localization;
use Oro\Bundle\LocaleBundle\Entity\LocalizedFallbackValue;
use Xngage\Bundle\DealersBundle\Entity\Dealer;

/**
 * @method File getImage1()
 * @method Dealer setImage1(File $image)
 * @method File getImage2()
 * @method Dealer setImage2(File $image)
 * @method File getImage3()
 * @method Dealer setImage3(File $image)
 * @method File getManagerImage()
 * @method Dealer setManagerImage(File $image)
 * @method LocalizedFallbackValue getSlug(Localization $localization = null)
 * @method LocalizedFallbackValue getDefaultSlug()
 * @method setDefaultSlug($slug)
 */
class ExtendDealer
{
    /**
     * Constructor
     *
     * The real implementation of this method is auto generated.
     *
     * IMPORTANT: If the derived class has own constructor it must call parent constructor.
     */
    public function __construct()
    {
    }
}