<?php

namespace Xngage\Bundle\DealersBundle\Form;

use Oro\Bundle\AddressBundle\Form\EventListener\AddressCountryAndRegionSubscriber;
use Oro\Bundle\AddressBundle\Form\Type\CountryType;
use Oro\Bundle\AddressBundle\Form\Type\RegionType;
use Oro\Bundle\FormBundle\Form\Type\CheckboxType;
use Symfony\Component\Form\AbstractType;
use Oro\Bundle\AttachmentBundle\Form\Type\ImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Oro\Bundle\FormBundle\Form\Extension\StripTagsExtension;
use Oro\Bundle\FormBundle\Form\Type\OroRichTextType;
use Oro\Bundle\LocaleBundle\Form\Type\LocalizedFallbackValueCollectionType;
use Oro\Bundle\RedirectBundle\Form\Type\LocalizedSlugWithRedirectType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Xngage\Bundle\DealersBundle\Entity\Dealer;
use Xngage\Bundle\DealersBundle\Entity\DealerDescription;
use Xngage\Bundle\DealersBundle\Entity\DealerTitle;
use Oro\Bundle\WebsiteBundle\Entity\Website;

class DealerType extends AbstractType
{
    /**
     * @var AddressCountryAndRegionSubscriber
     */
    private $countryAndRegionSubscriber;

    /**
     * @var UrlGeneratorInterface
     */
    protected $urlGenerator;

    public function __construct(
        AddressCountryAndRegionSubscriber $eventListener,
        UrlGeneratorInterface $urlGenerator
    ) {
        $this->countryAndRegionSubscriber = $eventListener;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventSubscriber($this->countryAndRegionSubscriber);

        $builder
            ->add('websites', EntityType::class, [
                'class' => Website::class,
                'label' => "Enabled Websites",
                'multiple' => true,
                'required' => false
            ])
            ->add('active', CheckboxType::class, [
                'required' => true
            ])
            ->add('phone', TextType::class)
            ->add('erpId',  TextType::class, [
                'required' => false
            ])
            ->add('website_url',  TextType::class)
            ->add('hours', TextType::class)
            ->add(
                'titles',
                LocalizedFallbackValueCollectionType::class,
                [
                    'label' => 'Titles',
                    'required' => true,
                    'value_class' => DealerTitle::class,
                    'entry_options' => ['constraints' => [
                        new NotBlank(['message' => 'Dealer Title should not be blank.'])]
                    ],
                ]
            )
            ->add(
                'descriptions',
                LocalizedFallbackValueCollectionType::class,
                [
                    'label' => 'Descriptions',
                    'required' => false,
                    'value_class' => DealerDescription::class,
                    'field' => 'text',
                    'entry_type' => OroRichTextType::class,
                    'entry_options' => [
                        'wysiwyg_options' => [
                            'autoRender' => false,
                            'elementpath' => true,
                            'resize' => true,
                            'height' => 200,
                        ],
                    ],
                    'use_tabs' => true,
                ]
            )
            ->add(
                'image1',
                ImageType::class,
                [
                    'label'    => 'Image 1',
                    'required' => false
                ]
            )
            ->add(
                'image2',
                ImageType::class,
                [
                    'label'    => 'Image 2',
                    'required' => false
                ]
            )
            ->add(
                'image3',
                ImageType::class,
                [
                    'label'    => 'Image 3',
                    'required' => false
                ]
            )
            ->add(
                'slugPrototypesWithRedirect',
                LocalizedSlugWithRedirectType::class,
                [
                    'label'    => 'URL Slug',
                    'required' => false,
                    'source_field' => 'titles',
                    'allow_slashes' => true,
                ]
            )
            ->add('address', TextType::class)
            ->add('address2', TextType::class)
            ->add('country', CountryType::class, array('required' => true, 'label' => 'oro.address.country.label'))
            ->add('city', TextType::class, array(
                'required' => false,
                'label' => 'oro.address.city.label',
                StripTagsExtension::OPTION_NAME => true,
            ))
            ->add('region', RegionType::class, array('required' => false, 'label' => 'oro.address.region.label'))
            ->add('postalCode', TextType::class, array(
                'required' => false,
                'label' => 'oro.address.postal_code.label',
                StripTagsExtension::OPTION_NAME => true,
            ))
            ->add(
                'region_text',
                HiddenType::class,
                array('required' => false, 'random_id' => true, 'label' => 'oro.address.region_text.label')
            )
            // Manager Info
            ->add('managerName', TextType::class)
            ->add('managerTitle', TextType::class)
            ->add('managerCellPhone', TextType::class)
            ->add('managerEmail', TextType::class)
            ->add('managerOfficePhone', TextType::class)
            ->add('latlng', GoogleMapsPickerType::class)
            ->add(
                'managerImage',
                ImageType::class,
                [
                    'label'    => 'Manager Image',
                    'required' => false
                ]
            )->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'preSetDataListener']);
    }

    public function preSetDataListener(FormEvent $event)
    {
        $dealer = $event->getData();

        if ($dealer instanceof Dealer && $dealer->getId()) {
            $url = $this->urlGenerator->generate(
                'xngage_dealer_get_changed_slugs',
                ['id' => $dealer->getId()]
            );

            $event->getForm()->add(
                'slugPrototypesWithRedirect',
                LocalizedSlugWithRedirectType::class,
                [
                    'label'    => 'URL Slug',
                    'required' => false,
                    'get_changed_slugs_url' => $url,
                    'source_field' => 'titles',
                    'allow_slashes' => true,
                ]
            );
        }
    }
}