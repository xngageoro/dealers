<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\DealersBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Oro\Bundle\FormBundle\Model\UpdateHandlerFacade;
use Oro\Bundle\RedirectBundle\Helper\ChangedSlugsHelper;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Xngage\Bundle\DealersBundle\Entity\Dealer;
use Oro\Bundle\ActionBundle\Helper\ContextHelper;
use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\CsrfProtection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;
use Xngage\Bundle\DealersBundle\Form\DealerType;

class DealerController extends AbstractController
{
    /**
     * List All Dealers
     * 
     * @Route("/", name="xngage_dealer_index")
     * @Template
     * 
     * @return array
     */
    public function indexAction()
    {
        return [
            "entity_class" => Dealer::class
        ];
    }

    /**
     * Create Dealer
     *
     * @Route("/create", name="xngage_dealer_create")
     * @Template("@XngageDealers/Dealer/form.html.twig")
     *
     * @return array|RedirectResponse
     */
    public function createAction()
    {
        $obj = new Dealer;
        return $this->update($obj);
    }

    /**
     * Edit Dealer
     *
     * @Route("/update/{id}", name="xngage_dealer_update", requirements={"id"="\d+"})
     * @Template("@XngageDealers/Dealer/form.html.twig")
     *
     * @param Dealer $entity
     *
     * @return array|RedirectResponse
     */
    public function updateAction(Dealer $entity)
    {
        return $this->update($entity);
    }

    /**
     * @param Dealer $entity
     *
     * @return array|RedirectResponse
     */
    protected function update(Dealer $entity)
    {
        $form = $this->createForm(DealerType::class, $entity);
        return $this->get(UpdateHandlerFacade::class)->update(
            $entity,
            $form,
            $this->get(TranslatorInterface::class)->trans('All Data Saved.')
        );
    }

    /**
     * Delete the mapper record
     *
     * @Route(
     *     "/delete/{id}",
     *      name="xngage_dealer_delete",
     *      methods={"DELETE"}
     * )
     * @Acl(
     *      id="xngage_dealer_delete",
     *      type="entity",
     *      class="XngageDealersBundle:Dealer",
     *      permission="DELETE"
     * )
     * @CsrfProtection()
     * @param Dealer $entity
     * @return JsonResponse
     */
    public function deleteAction(Dealer $entity)
    {
        $code = Response::HTTP_OK;
        $errors = new ArrayCollection();
        $message = '';
        try {
            $entityManager = $this->getDoctrine()->getManagerForClass(Dealer::class);
            $entityManager->remove($entity);
            $entityManager->flush();
        } catch (\Exception $e) {
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $message = $e->getMessage();
        }


        $response = [
            'successful' => $code === Response::HTTP_OK,
            'message' => $message,
            'messages' => $this->prepareMessages($errors),
            'refreshGrid' => $this->get(ContextHelper::class)->getActionData()->getRefreshGrid(),
            'flashMessages' => $this->get('session')->getFlashBag()->all()
        ];

        return new JsonResponse($response, $code);
    }

    /**
     * @param Collection $messages
     * @return array
     */
    protected function prepareMessages(Collection $messages)
    {
        $result = [];

        foreach ($messages as $message) {
            $result[] = $this->get(TranslatorInterface::class)->trans($message['message'], $message['parameters']);
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedServices()
    {
        return array_merge(
            parent::getSubscribedServices(),
            [
                UpdateHandlerFacade::class,
                TranslatorInterface::class,
                ContextHelper::class,
                ChangedSlugsHelper::class
            ]
        );
    }

    /**
     * @Route("/get-changed-urls/{id}", name="xngage_dealer_get_changed_slugs", requirements={"id"="\d+"})
     *
     * @param Dealer $dealer
     * @return JsonResponse
     */
    public function getChangedSlugsAction(Dealer $dealer)
    {
        return new JsonResponse($this->get(ChangedSlugsHelper::class)
            ->getChangedSlugsData($dealer, DealerType::class));
    }
}