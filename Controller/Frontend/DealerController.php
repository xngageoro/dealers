<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\DealersBundle\Controller\Frontend;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Oro\Bundle\FormBundle\Model\UpdateHandlerFacade;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Oro\Bundle\LayoutBundle\Annotation\Layout;
use Symfony\Component\HttpFoundation\Request;
use Xngage\Bundle\DealersBundle\Entity\Dealer;
use Oro\Bundle\ActionBundle\Helper\ContextHelper;
use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\CsrfProtection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Oro\Bundle\NavigationBundle\Annotation\TitleTemplate;
use Symfony\Contracts\Translation\TranslatorInterface;
use Xngage\Bundle\DealersBundle\Form\DealerType;

class DealerController extends AbstractController
{
    /**
     * @Route("/dealers/", name="xngage_dealer_frontend_index")
     * @TitleTemplate("Dealers")
     * @Layout()
     */
    public function showAction()
    {
        return [];
    }

    /**
     * @Route("/dealers/{id}", name="xngage_dealer_frontend_show", requirements={"id"="\d+"})
     * @TitleTemplate("Dealers")
     * @Layout()
     *
     * @param Dealer $dealer
     *
     * @return array
     */
    public function viewAction(Dealer $dealer)
    {
        return ['data' => ['dealer' => $dealer]];
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedServices()
    {
        return array_merge(
            parent::getSubscribedServices(),
            [
                UpdateHandlerFacade::class,
                TranslatorInterface::class,
                ContextHelper::class,
            ]
        );
    }
}