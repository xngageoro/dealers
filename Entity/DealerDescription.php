<?php

namespace Xngage\Bundle\DealersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\ConfigField;
use Oro\Bundle\LocaleBundle\Entity\AbstractLocalizedFallbackValue;

/**
 * Represents dealer description
 *
 * @ORM\Table(
 *      name="xngage_dealer_description",
 *      indexes={
 *          @ORM\Index(name="idx_xngage_dealer_s_descr_fallback", columns={"fallback"})
 *      }
 * )
 * @ORM\Entity
 * @Config
 */
class DealerDescription extends AbstractLocalizedFallbackValue
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "excluded"=false
     *          }
     *      }
     * )
     */
    protected $text;

    /**
     * @var Dealer
     *
     * @ORM\ManyToOne(targetEntity="Dealer", inversedBy="descriptions")
     * @ORM\JoinColumn(name="dealer_id", referencedColumnName="id", onDelete="CASCADE")
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "excluded"=true
     *          }
     *      }
     * )
     */
    protected $dealer;

    public function getDealer(): ?Dealer
    {
        return $this->dealer;
    }

    /**
     * @param null|Dealer $dealer
     * @return $this
     */
    public function setDealer(?Dealer $dealer): self
    {
        $this->dealer = $dealer;

        return $this;
    }
}
