<?php

namespace Xngage\Bundle\DealersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\ConfigField;
use Oro\Bundle\LocaleBundle\Entity\AbstractLocalizedFallbackValue;

/**
 * Represents Dealer title
 *
 * @ORM\Table(
 *      name="xngage_dealer_name",
 *      indexes={
 *          @ORM\Index(name="idx_xngage_dealer_fallback", columns={"fallback"}),
 *          @ORM\Index(name="idx_xngage_dealer_string", columns={"string"})
 *      }
 * )
 * @ORM\Entity
 * @Config
 */
class DealerTitle extends AbstractLocalizedFallbackValue
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="string", type="string", length=255, nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "excluded"=false
     *          }
     *      }
     * )
     */
    protected $string;

    /**
     * @var Dealer
     *
     * @ORM\ManyToOne(targetEntity="Dealer", inversedBy="titles")
     * @ORM\JoinColumn(name="dealer_id", referencedColumnName="id", onDelete="CASCADE")
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "excluded"=true
     *          }
     *      }
     * )
     */
    protected $dealer;

    public function getDealer(): ?Dealer
    {
        return $this->dealer;
    }

    /**
     * @param null|Dealer $dealer
     * @return $this
     */
    public function setDealer(?Dealer $dealer): self
    {
        $this->dealer = $dealer;

        return $this;
    }
}
