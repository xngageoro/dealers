<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion

namespace Xngage\Bundle\DealersBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\ConfigField;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use Oro\Bundle\EntityBundle\EntityProperty\DatesAwareInterface;
use Oro\Bundle\EntityBundle\EntityProperty\DatesAwareTrait;
use Oro\Bundle\EntityBundle\EntityProperty\DenormalizedPropertyAwareInterface;
use Xngage\Bundle\DealersBundle\Model\ExtendDealer;
use Oro\Bundle\LocaleBundle\Entity\FallbackTrait;
use Oro\Bundle\LocaleBundle\Entity\LocalizedFallbackValue;
use Oro\Bundle\RedirectBundle\Entity\SluggableInterface;
use Oro\Bundle\RedirectBundle\Entity\SluggableTrait;
use Oro\Bundle\RedirectBundle\Model\SlugPrototypesWithRedirect;
use Oro\Bundle\WebsiteBundle\Entity\Website;
use Xngage\Bundle\DealersBundle\Form\Validator\Constraints as XngageAssert;

/**
 * Dealer
 *
 * @ORM\Table(
 *      name="xngage_dealer",
 *      indexes={
 *              @ORM\Index(name="idx_xngage_dealer_default_title", columns={"title"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="Xngage\Bundle\DealersBundle\Entity\Repository\DealerRepository")
 * @ORM\AssociationOverrides({
 *      @ORM\AssociationOverride(
 *          name="slugPrototypes",
 *          joinTable=@ORM\JoinTable(
 *              name="xngage_dealer_slug_prototype",
 *              joinColumns={
 *                  @ORM\JoinColumn(name="dealer_id", referencedColumnName="id", onDelete="CASCADE")
 *              },
 *              inverseJoinColumns={
 *                  @ORM\JoinColumn(
 *                      name="localized_value_id",
 *                      referencedColumnName="id",
 *                      onDelete="CASCADE",
 *                      unique=true
 *                  )
 *              }
 *          )
 *      ),
 *     @ORM\AssociationOverride(
 *          name="slugs",
 *          joinTable=@ORM\JoinTable(
 *              name="xngage_dealer_slug",
 *              joinColumns={
 *                  @ORM\JoinColumn(name="dealer_id", referencedColumnName="id", onDelete="CASCADE")
 *              },
 *              inverseJoinColumns={
 *                  @ORM\JoinColumn(name="slug_id", referencedColumnName="id", unique=true, onDelete="CASCADE")
 *              }
 *          )
 *      )
 * })
 * @Config(
 *     routeName="xngage_dealer_index",
 *     routeCreate="xngage_dealer_create",
 *     routeUpdate="xngage_dealer_update",
 *     defaultValues={
 *          "dataaudit"={
 *              "auditable"=true
 *          },
 *          "form"={
 *              "form_type"="Xngage\Bundle\DealersBundle\Form\DealerType",
 *              "grid_name"="xngage-dealers-grid",
 *          },
 *         "security"={
 *             "type"="acl",
 *             "group_name"=""
 *         },
 *          "slug"={
 *              "source"="titles"
 *          }
 *     }
 * )
 * @ORM\HasLifecycleCallbacks()
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class Dealer extends ExtendDealer implements
    SluggableInterface,
    DatesAwareInterface,
    DenormalizedPropertyAwareInterface
{
    use FallbackTrait, SluggableTrait, DatesAwareTrait;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=false
     *          },
     *          "importexport"={
     *              "order"=1
     *          }
     *      }
     * )
     */
    protected $id;

    /**
     * @var Collection|DealerTitle[]
     *
     * @ORM\OneToMany(targetEntity="DealerTitle", mappedBy="dealer", cascade={"ALL"}, orphanRemoval=true)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          },
     *          "importexport"={
     *              "order"=2,
     *              "full"=true,
     *              "fallback_field"="string"
     *          }
     *      }
     * )
     */
    protected $titles;

    /**
     * @var Collection|DealerDescription[]
     *
     * @ORM\OneToMany(targetEntity="DealerDescription", mappedBy="dealer", cascade={"ALL"}, orphanRemoval=true)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          },
     *          "importexport"={
     *              "order"=3,
     *              "full"=true,
     *              "fallback_field"="string"
     *          }
     *      }
     * )
     */
    protected $descriptions;

    /**
     * @var Collection|LocalizedFallbackValue[]
     *
     * @Symfony\Component\Validator\Constraints\All(
     *     constraints = {
     *         @Oro\Bundle\RedirectBundle\Validator\Constraints\UrlSafeSlugPrototype(allowSlashes=true)
     *     }
     * )
     *
     * @ORM\ManyToMany(
     *      targetEntity="Oro\Bundle\LocaleBundle\Entity\LocalizedFallbackValue",
     *      cascade={"ALL"},
     *      orphanRemoval=true
     * )
     *
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "order"=4,
     *              "full"=true,
     *              "fallback_field"="string"
     *          }
     *      }
     * )
     */
    protected $slugPrototypes;

    /**
     * @var Website[]|Collection
     *
     * @ORM\ManyToMany(targetEntity="Oro\Bundle\WebsiteBundle\Entity\Website")
     * @ORM\JoinTable(
     *      name="xngage_dealer_website",
     *      joinColumns={
     *          @ORM\JoinColumn(name="dealer_id", referencedColumnName="id", onDelete="CASCADE")
     *      },
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(name="website_id", referencedColumnName="id", onDelete="CASCADE")
     *      }
     * )
     */
    private $websites;

    /**
     * @var int
     *
     * @ORM\Column(name="erp_id", type="string", nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "identity"=true,
     *              "order"=5
     *          }
     *      }
     * )
     */
    protected $erpId;

    /**
     *
     * @var bool
     * 
     * @ORM\Column(name="active", type="boolean", options={"default"=true})
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "order"=6
     *          }
     *      }
     * )
     */
    protected $active = true;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "excluded"=true
     *          }
     *      }
     * )
     */
    protected $title;

    /**
     * @var string
     * 
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "order"=7
     *          }
     *      }
     * )
     */
    protected $phone;

    /**
     * @var string
     * 
     * @ORM\Column(name="address_1", type="string", length=255, nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *             "order"=8
     *          }
     *      }
     * )
     */
    protected $address1;

    /**
     * @var string
     * 
     * @ORM\Column(name="address_2", type="string", length=255, nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "order"=9
     *          }
     *      }
     * )
     */
    protected $address2;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Oro\Bundle\AddressBundle\Entity\Country")
     * @ORM\JoinColumn(name="country_code", referencedColumnName="iso2_code")
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "order"=10,
     *              "short"=true
     *          }
     *      }
     * )
     */
    protected $country;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", length=255, nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "order"=11
     *          }
     *      }
     * )
     */
    protected $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "order"=12
     *          }
     *      }
     * )
     */
    protected $city;

    /**
     * @var Region
     *
     * @ORM\ManyToOne(targetEntity="Oro\Bundle\AddressBundle\Entity\Region")
     * @ORM\JoinColumn(name="region_code", referencedColumnName="combined_code")
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "order"=13,
     *              "short"=true
     *          }
     *      }
     * )
     */
    protected $region;

    /**
     * @var string
     *
     * @ORM\Column(name="region_text", type="string", length=255, nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "order"=14
     *          }
     *      }
     * )
     */
    protected $regionText;

    /**
     * @var string
     * 
     * @ORM\Column(name="website_url", type="string", length=255, nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "order"=15
     *          }
     *      }
     * )
     */
    protected $websiteUrl;

    /**
     * @var string
     * 
     * @ORM\Column(name="hours", type="string", length=255, nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "order"=16
     *          }
     *      }
     * )
     */
    protected $hours;

    /**
     * @var string
     * 
     * @ORM\Column(name="manager_name", type="string", length=255, nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "order"=17
     *          }
     *      }
     * )
     */
    protected $managerName;

    /**
     * @var string
     *
     * @ORM\Column(name="manager_title", type="string", length=255, nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "order"=17
     *          }
     *      }
     * )
     */
    protected $managerTitle;

    /**
     * @var string
     * 
     * @ORM\Column(name="manager_cell_phone", type="string", length=255, nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "order"=18
     *          }
     *      }
     * )
     */
    protected $managerCellPhone;

    /**
     * @var string
     * 
     * @ORM\Column(name="manager_email", type="string", length=255, nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "order"=19
     *          }
     *      }
     * )
     */
    protected $managerEmail;

    /**
     * @var string
     * 
     * @ORM\Column(name="manager_office_phoe", type="string", length=255, nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "order"=20
     *          }
     *      }
     * )
     */
    protected $managerOfficePhone;

    /**
     * @var float
     * @ORM\Column(name="lat", type="decimal", length=18, scale=12, precision=18)
     */
    protected $lat;

    /**
     * @var float
     * @ORM\Column(name="lng", type="decimal", length=18, scale=12, precision=18)
     */
    protected $lng;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @ConfigField(
     *      defaultValues={
     *          "entity"={
     *              "label"="oro.ui.created_at"
     *          },
     *          "importexport"={
     *              "excluded"=true
     *          }
     *      }
     * )
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @ConfigField(
     *      defaultValues={
     *          "entity"={
     *              "label"="oro.ui.updated_at"
     *          },
     *          "importexport"={
     *              "excluded"=true
     *          }
     *      }
     * )
     */
    protected $updatedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->titles = new ArrayCollection();
        $this->descriptions = new ArrayCollection();
        $this->slugPrototypes = new ArrayCollection();
        $this->websites = new ArrayCollection();
        $this->slugs = new ArrayCollection();
        $this->createdAt = new \DateTime('now', new \DateTimeZone('UTC'));
        $this->updatedAt = new \DateTime('now', new \DateTimeZone('UTC'));
        $this->slugPrototypesWithRedirect = new SlugPrototypesWithRedirect($this->slugPrototypes);
    }

    /**
     * @return DealerTitle
     */
    public function getDefaultTitle(): ?DealerTitle
    {
        return $this->getDefaultFallbackValue($this->titles);
    }

    /**
     * @return DealerDescription
     */
    public function getDefaultDescription(): ?DealerDescription
    {
        return $this->getDefaultFallbackValue($this->descriptions);
    }

    /**
     * @return LocalizedFallbackValue
     */
    public function getDefaultSlugPrototype(): ?LocalizedFallbackValue
    {
        return $this->getDefaultFallbackValue($this->slugPrototypes);
    }

    /**
     * Pre persist event handler
     *
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->updateDenormalizedProperties();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime('now', new \DateTimeZone('UTC'));
        $this->updateDenormalizedProperties();
    }

    public function updateDenormalizedProperties(): void
    {
        if (!$this->getDefaultTitle()) {
            throw new \RuntimeException('Dealer has to have a default title');
        }
        $this->title = $this->getDefaultTitle()->getString();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getDefaultTitle();
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultTitle($value)
    {
        $this->setDefaultFallbackValue($this->titles, $value, DealerTitle::class);
        $this->getDefaultTitle()->setDealer($this);

        return $this;
    }

    /**
     * @param DealerTitle $title
     *
     * @return $this
     */
    public function addTitle(DealerTitle $title)
    {
        if (!$this->titles->contains($title)) {
            $title->setDealer($this);
            $this->titles->add($title);
        }

        return $this;
    }

    /**
     * @param DealerTitle $title
     *
     * @return $this
     */
    public function removeTitle(DealerTitle $title)
    {
        if ($this->titles->contains($title)) {
            $this->titles->removeElement($title);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultDescription($value)
    {
        $this->setDefaultFallbackValue($this->descriptions, $value, DealerDescription::class);
        $this->getDefaultDescription()->setDealer($this);

        return $this;
    }

    /**
     * @return string
     */
    public function getManagerTitle()
    {
        return $this->managerTitle;
    }

    /**
     * @param string $managerTitle
     *
     * @return $this
     */
    public function setManagerTitle(string $managerTitle)
    {
        $this->managerTitle = $managerTitle;

        return $this;
    }

    /**
     * @param DealerDescription $description
     *
     * @return $this
     */
    public function addDescription(DealerDescription $description)
    {
        if (!$this->descriptions->contains($description)) {
            $description->setDealer($this);
            $this->descriptions->add($description);
        }

        return $this;
    }

    /**
     * @param DealerDescription $description
     *
     * @return $this
     */
    public function removeDescription(DealerDescription $description)
    {
        if ($this->descriptions->contains($description)) {
            $this->descriptions->removeElement($description);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getErpId()
    {
        return $this->erpId;
    }

    /**
     * @param int $id
     */
    public function setErpId($erpId)
    {
        $this->erpId = $erpId;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Set address 1
     *
     * @param string $address1
     * @return $this
     */
    public function setAddress($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address1;
    }

    /**
     * Set address 2
     *
     * @param string $address2
     * @return $this
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set region
     *
     * @param Region $region
     * @return $this
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Get code of region
     *
     * @return string
     */
    public function getRegionCode()
    {
        return $this->getRegion() ? $this->getRegion()->getCode() : '';
    }

    /**
     * Get name of region
     *
     * @return string
     */
    public function getRegionName()
    {
        return $this->getRegion() ? $this->getRegion()->getName() : $this->getRegionText();
    }

    /**
     * Get region or region string
     *
     * @return Region|string
     */
    public function getUniversalRegion()
    {
        if (!empty($this->regionText)) {
            return $this->regionText;
        }
        return $this->region;
    }

    /**
     * Set postal_code
     *
     * @param string $postalCode
     * @return $this
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postal_code
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set country
     *
     * @param Country $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Get name of country
     *
     * @return string
     */
    public function getCountryName()
    {
        return $this->getCountry() ? $this->getCountry()->getName() : '';
    }

    /**
     * Get country ISO3 code
     *
     * @return string
     */
    public function getCountryIso3()
    {
        return $this->getCountry() ? $this->getCountry()->getIso3Code() : '';
    }

    /**
     * Get country ISO2 code
     *
     * @return string
     */
    public function getCountryIso2()
    {
        return $this->getCountry() ? $this->getCountry()->getIso2Code() : '';
    }

    /**
     * Set website url
     *
     * @param string $websiteUrl
     * @return $this
     */
    public function setWebsiteUrl($websiteUrl)
    {
        $this->websiteUrl = $websiteUrl;

        return $this;
    }

    /**
     * Get hours
     *
     * @return string
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Set hours
     *
     * @param string $hours
     * @return $this
     */
    public function setHours(string $hours): static
    {
        $this->hours = $hours;

        return $this;
    }

    /**
     * Get manager name
     *
     * @return string
     */
    public function getManagerName()
    {
        return $this->managerName;
    }

    /**
     * Set manager name
     *
     * @param string $manager_name
     * @return $this
     */
    public function setManagerName($managerName)
    {
        $this->managerName = $managerName;

        return $this;
    }

    /**
     * Get manager cell phone
     *
     * @return string
     */
    public function getManagerCellPhone()
    {
        return $this->managerCellPhone;
    }

    /**
     * Set manager cell phone
     *
     * @param string $manager_name
     * @return $this
     */
    public function setManagerCellPhone($managerCellPhone)
    {
        $this->managerCellPhone = $managerCellPhone;

        return $this;
    }

    /**
     * Get website url
     *
     * @return string
     */
    public function getWebsiteUrl()
    {
        return $this->websiteUrl;
    }

    /**
     * Get the value of managerOfficePhone
     *
     * @return  string
     */
    public function getManagerOfficePhone()
    {
        return $this->managerOfficePhone;
    }

    /**
     * Set the value of managerOfficePhone
     *
     * @param  string  $managerOfficePhone
     *
     * @return  self
     */
    public function setManagerOfficePhone(string $managerOfficePhone)
    {
        $this->managerOfficePhone = $managerOfficePhone;

        return $this;
    }

    /**
     * Get the value of manager email
     *
     * @return  string
     */
    public function getManagerEmail()
    {
        return $this->managerEmail;
    }

    /**
     * Set the value of managerEmail
     *
     * @param string $managerEmail
     *
     * @return self
     */
    public function setManagerEmail(string $managerEmail)
    {
        $this->managerEmail = $managerEmail;

        return $this;
    }

    /**
     * Get the value of phone
     *
     * @return  string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set the value of phone
     *
     * @param  string  $phone
     *
     * @return  self
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get the value of titles
     *
     * @return  Collection|DealerTitle[]
     */
    public function getTitles()
    {
        return $this->titles;
    }

    /**
     * Set the value of titles
     *
     * @param  Collection|DealerTitle[]  $titles
     *
     * @return  self
     */
    public function setTitles($titles)
    {
        $this->titles = $titles;

        return $this;
    }

    /**
     * Get the value of descriptions
     *
     * @return  Collection|DealerDescription[]
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    /**
     * Set the value of descriptions
     *
     * @param  Collection|DealerDescription[]  $descriptions
     *
     * @return  self
     */
    public function setDescriptions($descriptions)
    {
        $this->descriptions = $descriptions;

        return $this;
    }

    /**
     * Get the value of regionText
     *
     * @return  string
     */
    public function getRegionText()
    {
        return $this->regionText;
    }

    /**
     * Set the value of regionText
     *
     * @param  string  $regionText
     *
     * @return  self
     */
    public function setRegionText(string $regionText)
    {
        $this->regionText = $regionText;

        return $this;
    }

    /**
     * Get the value of websites
     *
     * @return  Website[]|Collection
     */
    public function getWebsites()
    {
        return $this->websites;
    }

    /**
     * Set the value of websites
     *
     * @param  Website[]|Collection  $websites
     *
     * @return  self
     */
    public function setWebsites($websites)
    {
        $this->websites = $websites;

        return $this;
    }

    /**
     * @param Website $website
     *
     * @return $this
     */
    public function addWebsite(Website $website)
    {
        if (!$this->websites->contains($website)) {
            $this->websites->add($website);
        }

        return $this;
    }

    /**
     * @param Website $website
     *
     * @return $this
     */
    public function removeWebsite(Website $website)
    {
        if ($this->websites->contains($website)) {
            $this->websites->removeElement($website);
        }

        return $this;
    }

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     */
    public function setLat(float $lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * @return float
     */
    public function getLng(): float
    {
        return $this->lng;
    }

    /**
     * @param float $lng
     */
    public function setLng(float $lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * @return $this
     */
    public function setLatLng($latlng)
    {
        $this->setLat($latlng['lat']);
        $this->setLng($latlng['lng']);
        $this->setAddress($latlng['address']);
        return $this;
    }

    /**
     * @XngageAssert\LatLng()
     */
    public function getLatLng()
    {
        return [
            'lat' => $this->lat,
            'lng' => $this->lng,
            'address' => $this->address1
        ];

    }
}
