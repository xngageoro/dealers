<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\DealersBundle\EventListener;

use Oro\Bundle\EntityConfigBundle\Generator\SlugGenerator;
use Oro\Bundle\LocaleBundle\Entity\LocalizedFallbackValue;
use Xngage\Bundle\DealersBundle\Entity\Dealer;
use Xngage\Bundle\DealersBundle\Entity\DealerTitle;
use Xngage\Bundle\DealersBundle\ImportExport\Event\DealerStrategyAfterProcessEntityEvent;

/**
 * On Dealer import checks if the slug is empty and generates one from the dealer title
 */
class EmptySlugDealerStrategyEventListener
{
    /** @var SlugGenerator */
    private $slugGenerator;

    public function __construct(SlugGenerator $slugGenerator)
    {
        $this->slugGenerator = $slugGenerator;
    }

    public function onProcessAfter(DealerStrategyAfterProcessEntityEvent $event): void
    {
        $dealer = $event->getDealer();

        if ($dealer->getSlugPrototypes()->isEmpty()) {
            foreach ($dealer->getTitles() as $localizedTitle) {
                $this->addSlug($dealer, $localizedTitle);
            }
        }

        if (!$dealer->getDefaultSlugPrototype() && $dealer->getDefaultTitle()) {
            $this->addSlug($dealer, $dealer->getDefaultTitle());
        }
    }

    private function addSlug(Dealer $dealer, DealerTitle $localizedTitle): void
    {
        $localizedSlug = LocalizedFallbackValue::createFromAbstract($localizedTitle);
        $localizedSlug->setString($this->slugGenerator->slugify($localizedSlug->getString()));

        $dealer->addSlugPrototype($localizedSlug);
    }
}
