<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\DealersBundle\EventListener\Frontend;

use Oro\Bundle\DataGridBundle\Event\OrmResultAfter;
use Oro\Bundle\DataGridBundle\Event\OrmResultBeforeQuery;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Xngage\Bundle\DealersBundle\Entity\Dealer;
use Xngage\Bundle\DealersBundle\Provider\LatLongProvider;
use Oro\Bundle\WebsiteBundle\Manager\WebsiteManager;
use UnexpectedValueException;
use Xngage\Bundle\DealersBundle\Provider\DealersProvider;

class DealersDatagridListener
{
    /**
     * @var DoctrineHelper
     */
    protected $doctrineHelper;

    /**
     * @var LatLongProvider
     */
    protected LatLongProvider $latLongProvider;

    /** @var WebsiteManager */
    protected WebsiteManager $websiteManager;

    protected DealersProvider $dealersProvider;

    public function __construct(
        DoctrineHelper $doctrineHelper,
        LatLongProvider $latLongProvider,
        WebsiteManager $websiteManager,
        DealersProvider $dealersProvider
    ) {
        $this->doctrineHelper = $doctrineHelper;
        $this->latLongProvider = $latLongProvider;
        $this->websiteManager = $websiteManager;
        $this->dealersProvider = $dealersProvider;
    }

    public function onResultBeforeQuery(OrmResultBeforeQuery $event)
    {
        $website = $this->websiteManager->getCurrentWebsite();

        if (!$website) {
            throw new UnexpectedValueException('website needs to be available');
        }

        $qb = $event->getQueryBuilder();
        $qb->leftJoin('dealer.websites', 'w')
            ->andWhere('(w.id = :website_id or w.id IS NULL)')
            ->setParameter('website_id', $website->getId());

        $currentLatLong = $this->latLongProvider->findLatLongFromCustomerAddress();

        if ($currentLatLong) {
            $mi_or_km = $this->dealersProvider->getCurrentDistanceKmOrMi();
            $eqMultiply = $mi_or_km == 'miles' ? 1.609 : 1.0;
            $qb->addSelect('(GEO_DISTANCE(:lat, :long, dealer.lat, dealer.lng) * ' . $eqMultiply . ') as distance')
                ->addSelect("'" . $mi_or_km . "'" . " as distance_type")
                ->setParameter('lat', $currentLatLong['lat'])
                ->setParameter('long', $currentLatLong['long'])
                ->orderBy('distance');
        }
    }
    public function addDataSupport(OrmResultAfter $event)
    {
        $records = $event->getRecords();
        if (!$records) {
            return;
        }

        foreach ($records as $record) {
            $id = $record->getValue('id');
            $dealerRepository = $this->doctrineHelper->getEntityRepository(Dealer::class);
            /** @var Dealer $dealer */
            $dealer = $dealerRepository->find($id);

            if (!$dealer) {
                continue;
            }

            $record->setValue('dealerTitle', $dealer->getDefaultTitle()->getString());
            $record->setValue('stateCode', $dealer->getRegionCode());
            $record->setValue('countryIso2', $dealer->getCountryIso2());
            $record->setValue('address', $dealer->getAddress());

        }

    }
}
