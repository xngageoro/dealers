<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\DealersBundle\Layout\DataProvider;

use Doctrine\Common\Collections\Collection;
use Oro\Bundle\WebsiteBundle\Manager\WebsiteManager;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Xngage\Bundle\DealersBundle\Entity\Dealer;

class DealersDataProvider
{
    /**
     * @var RegistryInterface
     */
    protected $manager;

    /**
     * @var WebsiteManager
     */
    protected $websiteManager;

    /**
     * StockistsDataProvider constructor.
     * @param RegistryInterface $manager
     * @param WebsiteManager $websiteManager
     */
    public function __construct(
        RegistryInterface $manager,
        WebsiteManager $websiteManager
    ) {
        $this->manager = $manager;
        $this->websiteManager = $websiteManager;
    }

    /**
     * Fetches an array of the dealers assigned to the active website
     * @return Dealer[]|Collection
     */
    public function getDealers()
    {
        $website = $this->websiteManager->getCurrentWebsite();
        $repo = $this->manager->getRepository(Dealer::class);
        $dealersByWebsite = $repo->getDealersByWebsite($website);

        return $dealersByWebsite->toArray();
    }

    /**
     * Fetch a nested array of dealers indexed by group
     * @return array
     */
    public function getGroupedDealers($groupBy = 'state')
    {
        $dealers = $this->getDealers();

        $grouped = [];
        foreach ($dealers as $stockist) {
            $grouped[$stockist->getGroup()][] = $stockist;
        }


        return $grouped;
    }
}
