<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\DealersBundle\DependencyInjection;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;

class XngageDealersExtension extends Extension implements PrependExtensionInterface
{
    const ALIAS = "xngage_dealers";

    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('controllers.yml');
        $loader->load('services.yml');
        $loader->load('importexport.yml');

        $container->prependExtensionConfig($this->getAlias(), $config);
    }

    /**
     * {@inheritDoc}
     */
    public function prepend(ContainerBuilder $container)
    {
        $dbDriver = $container->getParameterBag()->get('database_driver');
        $index = str_replace('%',  '', $dbDriver);

        if ($container->getParameterBag()->has($index)) {
            $dbDriver = $container->getParameterBag()->get($index);
        }

        $db = str_contains($dbDriver, 'pgsql') ? "Postgresql" : "Mysql";

        $functionClassesNamespace = sprintf('Xngage\Bundle\DealersBundle\Doctrine\Query\%s', $db);

        $container->prependExtensionConfig('doctrine', [
            'orm' => [
                'dql' => [
                    'numeric_functions' => [
                        'GEO_DISTANCE' => sprintf('%s\GeoDistance', $functionClassesNamespace),
                    ],
                ],
            ],
        ]);
    }
}