define(function(require) {
    'use strict';

    const TextFilter = require('oro/filter/text-filter');
    const template = require('tpl-loader!xngagedealers/templates/filter/dealers-full-search-filter.html');

    const DealersFullSearchFilter = TextFilter.extend({
        /**
         * Template selector for filter criteria
         *
         * @property
         */
         template: template,
    });

    return DealersFullSearchFilter;
});
