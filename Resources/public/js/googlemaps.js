/* global google */
define(function(require) {
  'use strict';

  var XngageGoogleMapsView;
  var _ = require('underscore');
  var Backbone = require('backbone');
  var __ = require('orotranslation/js/translator');
  var localeSettings = require('orolocale/js/locale-settings');
  var LoadingMaskView = require('oroui/js/app/views/loading-mask-view');
  var BaseView = require('oroaddress/js/mapservice/googlemaps');
  var messenger = require('oroui/js/messenger');
  var $ = Backbone.$;
  const mediator = require('oroui/js/mediator');

  /**
   * @export  xngagedealers/js/googlemaps
   * @class   xngagedealers.XngageGoogleMaps
   * @extends BaseView
   */
  XngageGoogleMapsView = BaseView.extend({
    options: {
      mapOptions: {
        zoom: 4,
        mapTypeControl: true,
        panControl: false,
        zoomControl: true
      },
      apiVersion: '3.exp',
      sensor: false,
      apiKey: null,
      showWeather: true
    },

    errorMessage: null,

    geocoder: null,

    mapRespondingTimeout: 2000,

    loadingMask: null,

    bounds: null, // Keep track of the bounding zoom for the map
    markers: {},
    infoWindow: null,
    currentPos: {},

    /**
     * @inheritDoc
     */
    constructor: function XngageGoogleMapsView() {
      XngageGoogleMapsView.__super__.constructor.apply(this, arguments);
    },

    /**
     * @inheritDoc
     */
    initialize: function(options) {
      var self = this;

      self.options = _.defaults(options || {},
        _.pick(localeSettings.settings, ['apiKey']),
        self.options
      );
      self.$mapsContainer = $('<div class="map-visual"/>').appendTo(self.$el);
      self.loadingMask = new LoadingMaskView({container: self.$el});

      // If there is no addresses data, then display error message
      if (!JSON.parse(options.addresses)) {
        this.mapLocationUnknown();
        return;
      }
      let parsedAddresses = JSON.parse(options.addresses);

      if (Object.keys(parsedAddresses).length > 0) {
        this.loadGoogleMaps(function() { self.populateMap(parsedAddresses) });
      }

      this.attachListeners();
    },

    /**
     * Attach listeners to elements that will control which marker is highlighted
     *
     * @returns {null}
     */
    attachListeners: function() {
      var self = this;

      // When a dealers name is clicked, pan and zoom to that location on the map
      $('.dealers').on('click', '[data-js="show-on-map"]', function(e) {
        var $locationName = $(e.target);
        var locationId = $locationName.closest('[data-location-id]').data().locationId;

        self.panZoomToMarker(self.markers[locationId]);
      });

      $(document).on('xngageDealers:clearMarkers', function (event) {
        self.clearMapMakers();
        // Announce that all markers have been cleared, so it's "safe" to add new ones, without running the risk of them
        // being accidentally removed as well
        $(document).trigger('xngageDealers:markersCleared');
      });

      // Listen for custom event, which will provide data of marker/s to add to the map
      $(document).on('xngageDealers:addMarker', function (event, data) {
        self.addMarkerToMap(data.id, data.location, data.label);
      });

      mediator.on('grid_load:complete', this.onLocationsChanged, this);
    },

    onLocationsChanged: function(collection, $grid) {
      let self = this;
      let parsedAddresses = collection.models.map((item) => {
        return {
          id: item.get('id'),
          lat: item.get('lat'),
          long: item.get('lng'),
          label: item.get('title')
        }
      })

      if (Object.keys(this.markers).length > 0) {
        this.clearMapMakers();
      }

      this.loadGoogleMaps(function() { self.populateMap(parsedAddresses) });
    },

    /**
     * Load the Google Maps script
     *
     * @param {function} callback The next steps after the Google Maps script has loaded
     *
     * @returns {null}
     */
    loadGoogleMaps: function(callback) {

      if (typeof google !== 'undefined') {
        callback();
        return;
      }

      var self = this;
      var googleMapsSettings = 'sensor=' + (self.options.sensor ? 'true' : 'false');

      if (self.options.showWeather) {
        googleMapsSettings += '&libraries=weather';
      }

      if (self.options.apiKey) {
        googleMapsSettings += '&key=' + self.options.apiKey;
      }

      $.ajax({
        url: window.location.protocol + '//www.google.com/jsapi',
        dataType: 'script',
        cache: true,
        success: function() {
          google.load('maps', self.options.apiVersion, {
            other_params: googleMapsSettings,
            callback: function() {
              self.createMapsInstance();
              self.createBoundsInstance();
              callback();
            }
          });
        }
      });
    },

    /**
     * Create an instance of the Google Maps library
     *
     * @returns {null}
     */
    createMapsInstance: function() {
      var self = this;

      self.map = new google.maps.Map(
        document.querySelector('.map-visual'),
        _.extend({}, self.options.mapOptions)
      );
    },

    /**
     * Create an instance of the LatLngBounds library
     * This needs to be done after the Google scripts have loaded
     *
     * @returns {null}
     */
    createBoundsInstance: function() {
      var self = this;

      self.bounds = new google.maps.LatLngBounds();
    },

    /**
     * Loop through the provided addresses and call function to add them to Google Map instance
     *
     * @param {array} addresses Array of address objects
     *
     * @returns {null}
     */
    populateMap: function(addresses) {
      var self = this;

      Object.keys(addresses).forEach(function(index) {
        var address = addresses[index];
        self.addressCount++;

        // If there isn't lat/lng values set, then geocode the address
        if (!address.lat || !address.long) {
          self.geocodeAndAdd(address);
          return;
        }

        // Use existing lat/lng values to place the marker on the map
        var location = new google.maps.LatLng(address.lat, address.long);
        self.addMarkerToMap(address.id, location, address.label)
      });
    },

    /**
     * Get an instance of the Google Geocoder library
     *
     * @returns {google.maps.Geocoder}
     */
    getGeocoder: function() {
      var self = this;

      if (_.isUndefined(self.geocoder) || _.isNull(self.geocoder)) {
        self.geocoder = new google.maps.Geocoder();
      }
      return self.geocoder;
    },

    /**
     * Geocode address and add it to the Google Maps instance
     *
     * @param address
     *
     * @returns {null}
     */
    geocodeAndAdd: function(address) {
      var self = this;

      this.getGeocoder().geocode({address: address.address}, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          self.addMarkerToMap(address.id, results[0].geometry.location, address.label)
        } else {
          self.mapLocationUnknown();
        }
      });
    },

    /**
     * Add a single marker to the Google Maps instance
     *
     * @param {int}    id       ID of the address
     * @param {Object} location Location object
     * @param {string} label    Label to display with the marker
     *
     * @returns {google.maps.Marker}
     */
    addMarkerToMap: function(id, location, label) {
      var self = this;

      var marker = new google.maps.Marker({
        draggable: false,
        map: this.map,
        position: location,
        title: label,
        optimized: true
      });

      // Store the marker with it's ID for later reference
      self.markers[id] = marker;

      var infoWindow = new google.maps.InfoWindow({
        content: label
      });

      // Very simple Info Window displaying the label of the marker
      marker.addListener('click', function() {
        self.infoWindow&&self.infoWindow.close();
        infoWindow.open(self.map, marker);
        self.infoWindow = infoWindow;
      });

      // Update the zoom of the map to include this new marker
      self.bounds.extend(marker.getPosition());
      self.map.fitBounds(self.bounds);
      var listener = google.maps.event.addListener(self.map, "idle", function() { 
        if (self.map.getZoom() > 16) self.map.setZoom(16); 
        google.maps.event.removeListener(listener); 
      });
    },

    /**
     * Remove all markers from the map
     * This will likely be called after a new search, so only new results are shown, not previous results as well
     *
     * @returns {null}
     */
    clearMapMakers: function() {
      var self = this;

      Object.keys(self.markers).forEach(function (markerId) {
        var marker = self.markers[markerId];
        marker.setMap(null);
      });

      self.markers = [];
    },

    /**
     * Zoom the Map to show a specific marker
     *
     * @param {google.maps.Marker} marker The marker to zoom too
     *
     * @returns {null}
     */
    panZoomToMarker: function(marker) {
      var self = this;

      var latLng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
      self.map.panTo(latLng);
      self.map.setZoom(17);
    },

    /**
     * Handle the event when there are no map locations to display
     *
     * @returns {null}
     */
    mapLocationUnknown: function() {
      var self = this;

      $('.map-visual').hide();
      self.addErrorMessage(__('map.unknown.location'));
      self.loadingMask.hide();
    },

    /**
     * Display an error message
     *
     * @param {string} message The message to display
     * @param {string} type    Optional. Type of message to display. Default: 'warning'
     *
     * @returns {null}
     */
    addErrorMessage: function(message, type) {
      var self = this;

      self.removeErrorMessage();
      self.errorMessage = messenger.notificationFlashMessage(
        type || 'warning',
        message || __('map.unknown.unavailable'),
        {
          container: self.$el,
          hideCloseButton: true,
          insertMethod: 'prependTo'
        }
      );
    },

    /**
     * Remove an error message
     *
     * @returns {null}
     */
    removeErrorMessage: function() {
      var self = this;

      if (_.isNull(self.errorMessage)) {
        return;
      }
      messenger.clear(self.errorMessage.namespace, {
        container: self.$el
      });

      delete self.errorMessage;
    }
  });

  return XngageGoogleMapsView;
});

