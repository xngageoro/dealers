(function ($) {

    function GoogleMapType(settings, map_el) {

        var settings = $.extend({
            search_input_el: null,
            search_action_el: null,
            search_error_el: null,
            'current_position_el': null,
            'default_lat': '1',
            'default_lng': '-1',
            'default_zoom': 5,
            'lat_field': null,
            'lng_field': null,
            'addr_field': null,
            'callback': function (location, gmap) {
            },
            'error_callback': function (status) {
                this.settings.search_error_el.text(status);
            },
        }, settings);

        this.settings = settings;

        this.map_el = map_el;

        this.geocoder = new google.maps.Geocoder();

    }

    GoogleMapType.prototype = {
        initMap: function (center) {

            var center = new google.maps.LatLng(this.settings.default_lat, this.settings.default_lng);

            var mapOptions = {
                zoom: this.settings.default_zoom,
                center: center,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var $this = this;

            this.map = new google.maps.Map(this.map_el[0], mapOptions);

            this.addMarker(center);

            google.maps.event.addListener(this.marker, "dragend", function (event) {

                var point = $this.marker.getPosition();
                $this.map.panTo(point);
                $this.updateLocation(point);
                $this.updateAddress(point);
            });

            google.maps.event.addListener(this.map, 'click', function (event) {
                $this.insertMarker(event.latLng);
            });

            this.settings.search_action_el.click($.proxy(this.searchAddress, $this));

            this.settings.current_position_el.click($.proxy(this.currentPosition, $this));
        },

        searchAddress: function (e) {
            e.preventDefault();
            var $this = this;
            var address = this.settings.search_input_el.val();
            this.geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    $this.map.setCenter(results[0].geometry.location);
                    $this.map.setZoom(16);
                    $this.insertMarker(results[0].geometry.location);
                    var address = $this.getAddress(results[0]['address_components']);
                    $this.setAddress(address)
                } else {
                    $this.settings.error_callback.call($this, status);
                }
            });
        },

        updateAddress: function (location) {
            var $this = this;
            this.geocoder.geocode({'location': location}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        var address = $this.getAddress(results[0]['address_components']);
                        $this.setAddress(address);
                    }
                } else {
                    $this.settings.error_callback.call($this, status);
                }
            });
        },

        getAddress: function (elements) {
            var addr = {};
            elements.forEach((e) => {
                if (e['types'] && e['types'].includes('street_number')) addr['street_number'] = e['long_name'];
                if (e['types'] && e['types'].includes('route')) addr['route'] = e['long_name'];
                if (e['types'] && e['types'].includes('locality')) addr['city'] = e['long_name'];
                if (e['types'] && e['types'].includes('administrative_area_level_1')) addr['state'] = {
                    long: e['long_name'],
                    short: e['short_name']
                };
                if (e['types'] && e['types'].includes('country')) addr['country'] = {
                    long: e['long_name'],
                    short: e['short_name']
                };
                if (e['types'] && e['types'].includes('postal_code')) addr['postal_code'] = e['long_name'];

            });
            return addr;
        },

        setAddress: function (address) {
            if (this.settings.addr_field) {
                this.settings.addr_field.val(
                    address.street_number + ' ' + address.route
                )
            }
            $('input[name=\'dealer[address]\']').val(address.street_number + ' ' + address.route)
            $("input[name='dealer[city]']").val(address.city);
            $("input[name='dealer[postalCode]']").val(address.postal_code);
        },

        currentPosition: function (e) {
            e.preventDefault();
            var $this = this;

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(
                    function (position) {
                        var clientPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        $this.insertMarker(clientPosition);
                        $this.map.setCenter(clientPosition);
                        $this.map.setZoom(16);
                    },
                    function (error) {
                        $this.settings.error_callback.call($this, error);
                    }
                );
            } else {
                $this.settings.search_error_el.text('Your browser does not support geolocation');
            }

        },

        updateLocation: function (location) {
            this.settings.lat_field.val(location.lat());
            this.settings.lng_field.val(location.lng());
            this.settings.callback(location, this);
        },

        addMarker: function (center) {
            if (this.marker) {
                this.marker.setMap(this.map);
                this.marker.setPosition(center);
            } else {
                this.marker = new google.maps.Marker({
                    map: this.map,
                    position: center,
                    draggable: true
                });
            }
        },

        insertMarker: function (position) {
            this.removeMarker();

            this.addMarker(position);

            this.updateLocation(position);

            this.updateAddress(position);
        },
        removeMarker: function () {
            if (this.marker != undefined) {
                this.marker.setMap(null);
            }
        }

    }

    $.fn.xngageGoogleMapType = function (settings) {

        settings = $.extend({}, $.fn.xngageGoogleMapType.defaultSettings, settings || {});

        return this.each(function () {
            var map_el = $(this);

            map_el.data('map', new GoogleMapType(settings, map_el));

            map_el.data('map').initMap();

        });

    };

    $.fn.xngageGoogleMapType.defaultSettings = {
        'search_input_el': null,
        'search_action_el': null,
        'search_error_el': null,
        'current_position_el': null,
        'default_lat': '1',
        'default_lng': '-1',
        'default_zoom': 5,
        'lat_field': null,
        'lng_field': null,
        'addr_field': null,
        'callback': function (location, gmap) {
        },
        'error_callback': function (status) {
            this.settings.search_error_el.text(status);
        }
    }

})(jQuery);